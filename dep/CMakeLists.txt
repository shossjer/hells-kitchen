cmake_minimum_required(VERSION 3.0)

include(ExternalProject)

set(INSTALL_DIR "${PROJECT_SOURCE_DIR}")


set(CONFIG_ARGS "")
list(APPEND CONFIG_ARGS "-DCMAKE_INSTALL_PREFIX=${INSTALL_DIR}")
if (MSVC)
  list(APPEND CONFIG_FLAGS_DEBUG "/MTd /Zi /Ob0 /Od /RTC1")
  list(APPEND CONFIG_ARGS "-DCMAKE_C_FLAGS_DEBUG=${CONFIG_FLAGS_DEBUG}")
  list(APPEND CONFIG_FLAGS_MINSIZEREL "/MT /O1 /Ob1 /DNDEBUG")
  list(APPEND CONFIG_ARGS "-DCMAKE_C_FLAGS_MINSIZEREL=${CONFIG_FLAGS_MINSIZEREL}")
  list(APPEND CONFIG_FLAGS_RELWITHDEBINFO "/MT /Zi /O2 /Ob1 /DNDEBUG")
  list(APPEND CONFIG_ARGS "-DCMAKE_C_FLAGS_RELWITHDEBINFO=${CONFIG_FLAGS_RELWITHDEBINFO}")
  list(APPEND CONFIG_FLAGS_RELEASE "/MT /O2 /Ob2 /DNDEBUG")
  list(APPEND CONFIG_ARGS "-DCMAKE_C_FLAGS_RELEASE=${CONFIG_FLAGS_RELEASE}")
endif ()

ExternalProject_Add(
  zlib
  PREFIX "${PROJECT_SOURCE_DIR}/downloads/zlib"
  URL "http://zlib.net/zlib-1.2.11.tar.gz"
  TIMEOUT 10
  CMAKE_ARGS "${CONFIG_ARGS}"
  LOG_DOWNLOAD ON
  )


set(CONFIG_ARGS "")
list(APPEND CONFIG_ARGS "-DCMAKE_INSTALL_PREFIX=${INSTALL_DIR}")
list(APPEND CONFIG_ARGS "-DCMAKE_PREFIX_PATH=${INSTALL_DIR}")
if (MSVC)
  list(APPEND CONFIG_FLAGS_DEBUG "/MTd /Zi /Ob0 /Od /RTC1")
  list(APPEND CONFIG_ARGS "-DCMAKE_C_FLAGS_DEBUG=${CONFIG_FLAGS_DEBUG}")
  list(APPEND CONFIG_FLAGS_MINSIZEREL "/MT /O1 /Ob1 /DNDEBUG")
  list(APPEND CONFIG_ARGS "-DCMAKE_C_FLAGS_MINSIZEREL=${CONFIG_FLAGS_MINSIZEREL}")
  list(APPEND CONFIG_FLAGS_RELWITHDEBINFO "/MT /Zi /O2 /Ob1 /DNDEBUG")
  list(APPEND CONFIG_ARGS "-DCMAKE_C_FLAGS_RELWITHDEBINFO=${CONFIG_FLAGS_RELWITHDEBINFO}")
  list(APPEND CONFIG_FLAGS_RELEASE "/MT /O2 /Ob2 /DNDEBUG")
  list(APPEND CONFIG_ARGS "-DCMAKE_C_FLAGS_RELEASE=${CONFIG_FLAGS_RELEASE}")
endif ()

ExternalProject_Add(
  libpng
  DEPENDS zlib
  PREFIX "${PROJECT_SOURCE_DIR}/downloads/libpng"
  URL "https://sourceforge.net/projects/libpng/files/libpng16/older-releases/1.6.30/libpng-1.6.30.tar.xz/download"
  TIMEOUT 10
  CMAKE_ARGS "${CONFIG_ARGS}"
  LOG_DOWNLOAD ON
  )


set(CONFIG_ARGS "")
list(APPEND CONFIG_ARGS "-DCMAKE_INSTALL_PREFIX=${INSTALL_DIR}")
if (MSVC)
  list(APPEND CONFIG_FLAGS_DEBUG "/MTd /Zi /Ob0 /Od /RTC1")
  list(APPEND CONFIG_ARGS "-DCMAKE_C_FLAGS_DEBUG=${CONFIG_FLAGS_DEBUG}")
  list(APPEND CONFIG_FLAGS_MINSIZEREL "/MT /O1 /Ob1 /DNDEBUG")
  list(APPEND CONFIG_ARGS "-DCMAKE_C_FLAGS_MINSIZEREL=${CONFIG_FLAGS_MINSIZEREL}")
  list(APPEND CONFIG_FLAGS_RELWITHDEBINFO "/MT /Zi /O2 /Ob1 /DNDEBUG")
  list(APPEND CONFIG_ARGS "-DCMAKE_C_FLAGS_RELWITHDEBINFO=${CONFIG_FLAGS_RELWITHDEBINFO}")
  list(APPEND CONFIG_FLAGS_RELEASE "/MT /O2 /Ob2 /DNDEBUG")
  list(APPEND CONFIG_ARGS "-DCMAKE_C_FLAGS_RELEASE=${CONFIG_FLAGS_RELEASE}")
endif ()

ExternalProject_Add(
  nlohmann-json
  PREFIX "${PROJECT_SOURCE_DIR}/downloads/nlohmann-json"
  GIT_REPOSITORY "https://github.com/nlohmann/json.git"
  GIT_TAG "ce0b3fe5a334567825a554a08b76c725d0790500"
  TIMEOUT 10
  CMAKE_ARGS "${CONFIG_ARGS}"
  LOG_DOWNLOAD ON
  )


set(CONFIG_ARGS "")
list(APPEND CONFIG_ARGS "-DCMAKE_INSTALL_PREFIX=${INSTALL_DIR}")
if (MSVC)
  list(APPEND CONFIG_FLAGS_DEBUG "/MTd /Zi /Ob0 /Od /RTC1")
  list(APPEND CONFIG_ARGS "-DCMAKE_C_FLAGS_DEBUG=${CONFIG_FLAGS_DEBUG}")
  list(APPEND CONFIG_FLAGS_MINSIZEREL "/MT /O1 /Ob1 /DNDEBUG")
  list(APPEND CONFIG_ARGS "-DCMAKE_C_FLAGS_MINSIZEREL=${CONFIG_FLAGS_MINSIZEREL}")
  list(APPEND CONFIG_FLAGS_RELWITHDEBINFO "/MT /Zi /O2 /Ob1 /DNDEBUG")
  list(APPEND CONFIG_ARGS "-DCMAKE_C_FLAGS_RELWITHDEBINFO=${CONFIG_FLAGS_RELWITHDEBINFO}")
  list(APPEND CONFIG_FLAGS_RELEASE "/MT /O2 /Ob2 /DNDEBUG")
  list(APPEND CONFIG_ARGS "-DCMAKE_C_FLAGS_RELEASE=${CONFIG_FLAGS_RELEASE}")
endif ()

ExternalProject_Add(
  catch
  PREFIX "${PROJECT_SOURCE_DIR}/downloads/catch"
  GIT_REPOSITORY "https://github.com/philsquared/Catch.git"
  GIT_TAG "Catch1.x"
  TIMEOUT 10
  CMAKE_ARGS "${CONFIG_ARGS}"
  LOG_DOWNLOAD ON
  )


set(CONFIG_ARGS "")
list(APPEND CONFIG_ARGS "-DCMAKE_INSTALL_PREFIX=${INSTALL_DIR}")
if (MSVC)
  list(APPEND CONFIG_FLAGS_DEBUG "/MTd /Zi /Ob0 /Od /RTC1")
  list(APPEND CONFIG_ARGS "-DCMAKE_C_FLAGS_DEBUG=${CONFIG_FLAGS_DEBUG}")
  list(APPEND CONFIG_FLAGS_MINSIZEREL "/MT /O1 /Ob1 /DNDEBUG")
  list(APPEND CONFIG_ARGS "-DCMAKE_C_FLAGS_MINSIZEREL=${CONFIG_FLAGS_MINSIZEREL}")
  list(APPEND CONFIG_FLAGS_RELWITHDEBINFO "/MT /Zi /O2 /Ob1 /DNDEBUG")
  list(APPEND CONFIG_ARGS "-DCMAKE_C_FLAGS_RELWITHDEBINFO=${CONFIG_FLAGS_RELWITHDEBINFO}")
  list(APPEND CONFIG_FLAGS_RELEASE "/MT /O2 /Ob2 /DNDEBUG")
  list(APPEND CONFIG_ARGS "-DCMAKE_C_FLAGS_RELEASE=${CONFIG_FLAGS_RELEASE}")
endif ()

ExternalProject_Add(
  freetype
  PREFIX "${PROJECT_SOURCE_DIR}/downloads/freetype"
  URL "https://download.savannah.gnu.org/releases/freetype/freetype-2.8.1.tar.gz"
  TIMEOUT 10
  CMAKE_ARGS "${CONFIG_ARGS}"
  LOG_DOWNLOAD ON
  )


file(DOWNLOAD "https://www.khronos.org/registry/OpenGL/api/GL/glext.h" "${PROJECT_SOURCE_DIR}/downloads/glext/src/glext.h" TIMEOUT 10)
if (NOT EXISTS "${PROJECT_SOURCE_DIR}/include/GL/glext.h")
	file(INSTALL "${PROJECT_SOURCE_DIR}/downloads/glext/src/glext.h" DESTINATION "${PROJECT_SOURCE_DIR}/include/GL")
endif ()

file(DOWNLOAD "https://www.khronos.org/registry/EGL/api/KHR/khrplatform.h" "${PROJECT_SOURCE_DIR}/downloads/glext/src/khrplatform.h" TIMEOUT 10)
if (NOT EXISTS "${PROJECT_SOURCE_DIR}/include/KHR/khrplatform.h")
	file(INSTALL "${PROJECT_SOURCE_DIR}/downloads/glext/src/khrplatform.h" DESTINATION "${PROJECT_SOURCE_DIR}/include/KHR")
endif ()
