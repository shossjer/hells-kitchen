
#ifndef GAMEPLAY_GAMEMENU_HPP
#define GAMEPLAY_GAMEMENU_HPP

namespace gameplay
{
	namespace gamemenu
	{
		void create();
		void destroy();
	}
}

#endif /* GAMEPLAY_GAMEMENU_HPP */
